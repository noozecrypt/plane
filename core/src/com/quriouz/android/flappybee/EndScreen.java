package com.quriouz.android.flappybee;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;


/**
 * Created by good on 10-06-2017.
 */

public class EndScreen extends ScreenAdapter {
    private static final float WORLD_WIDTH = 1080;
    private static final float WORLD_HEIGHT = 1920;

    private Stage stage;

    private BitmapFont bitmapFont;

    private Texture backgroundTexture;
    private Texture gameOverTexture;
    private  Texture yourScoreTexture;
    private Texture highScoreTexture;
    private Texture restartTexture;
    private Texture restartPressTexture;
    private Texture homeTexture;
    private Texture homePressTexture;
    private Texture boardTexture;
    private Sound hornSound;

    private final FlappyBee game;

    private Music bgMusic;

    private int yourScore, highScore;


    Music sadMusic;


    public EndScreen(FlappyBee game, int yourScore,int highScore, Music bgMusic) {
        this.game = game;
        this.highScore = highScore;
        this.yourScore = yourScore;
        this.bgMusic = bgMusic;
        sadMusic = game.getAssetManager().get("sounds/sad.mp3");
        sadMusic.play();
        sadMusic.setVolume(1.0f);
        sadMusic.setLooping(true);
    }

    @Override
    public void show() {
        stage = new Stage(new FitViewport(WORLD_WIDTH,WORLD_HEIGHT));
        Gdx.input.setInputProcessor(stage);

        backgroundTexture = game.getAssetManager().get("bg.png");
        gameOverTexture = game.getAssetManager().get("gameOver.png");
        highScoreTexture = game.getAssetManager().get("highScore.png");
        yourScoreTexture = game.getAssetManager().get("yourScore.png");
        restartTexture = game.getAssetManager().get("restart.png");
        restartPressTexture = game.getAssetManager().get("restartPress.png");
        homeTexture = game.getAssetManager().get("home.png");
        homePressTexture = game.getAssetManager().get("homePress.png");
        boardTexture = game.getAssetManager().get("board.png");
        hornSound = game.getAssetManager().get("sounds/horn.mp3");


        bitmapFont = new BitmapFont(Gdx.files.internal("score.fnt"));

        Image background = new Image(backgroundTexture);
        stage.addActor(background);
        background.setFillParent(true);


        Image scoreBoard = new Image(boardTexture);
        scoreBoard.setPosition(WORLD_WIDTH / 2, WORLD_HEIGHT /2 , Align.center);
        stage.addActor(scoreBoard);

        Image gameOver = new Image(new TextureRegionDrawable(new
                TextureRegion(gameOverTexture)));
        gameOver.setPosition(WORLD_WIDTH /2, 3 * WORLD_HEIGHT / 4+150,
                Align.center);
        stage.addActor(gameOver);


        Image yourScoreImage = new Image(new TextureRegionDrawable(new
                TextureRegion(yourScoreTexture)));
        yourScoreImage.setPosition( WORLD_WIDTH / 2, WORLD_HEIGHT /4+300, Align.center);
        stage.addActor(yourScoreImage);


        Image highScoreImage = new Image(new TextureRegionDrawable(new
                TextureRegion(highScoreTexture)));
        highScoreImage.setPosition( WORLD_WIDTH / 2, WORLD_HEIGHT /2+200 , Align.center);
        stage.addActor(highScoreImage);


        Label yourScoreLabel = new Label(Integer.toString(yourScore),new Label.LabelStyle(bitmapFont, Color.BLACK));
        yourScoreLabel.setPosition(WORLD_WIDTH / 2, WORLD_HEIGHT /4+150, Align.center);
        stage.addActor(yourScoreLabel);


        Label highScoreLabel = new Label(Integer.toString(highScore),new Label.LabelStyle(bitmapFont, Color.BLACK));
        highScoreLabel.setPosition(WORLD_WIDTH / 2, WORLD_HEIGHT /2+50, Align.center);
        stage.addActor(highScoreLabel);


        ImageButton restartButton =  new ImageButton(new TextureRegionDrawable(new
                TextureRegion(restartTexture)), new TextureRegionDrawable(new
                TextureRegion(restartPressTexture)));
        restartButton.setPosition(WORLD_WIDTH /4*3, WORLD_HEIGHT /4-400);
        restartButton.addListener(new ActorGestureListener(){
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                sadMusic.setVolume(0.0f);
                sadMusic.stop();
                bgMusic.play();
                hornSound.play(1.0f);
                game.setScreen(new GameScreen(game,bgMusic));
                stage.dispose();
            }

        });
        stage.addActor(restartButton);


        ImageButton homeButton =  new ImageButton(new TextureRegionDrawable(new
                TextureRegion(homeTexture)), new TextureRegionDrawable(new
                TextureRegion(homePressTexture)));
        homeButton.setPosition(WORLD_WIDTH/16, WORLD_HEIGHT /4-400);
        homeButton.addListener(new ActorGestureListener(){
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                sadMusic.setVolume(0.0f);
                sadMusic.stop();
                game.setScreen(new StartScreen(game));
                stage.dispose();
            }

        });
        stage.addActor(homeButton);




    }

    public void resize(int width, int height) {
        stage.getViewport().update(width,height,true);
    }

    public void render(float delta){
        stage.act(delta);
        stage.draw();
    }


    @Override
    public void dispose() {
        backgroundTexture.dispose();
        highScoreTexture.dispose();
        yourScoreTexture.dispose();
        gameOverTexture.dispose();
        restartTexture.dispose();
        restartTexture.dispose();
        homePressTexture.dispose();
        homeTexture.dispose();
        boardTexture.dispose();
        sadMusic.dispose();
        stage.dispose();
    }

}
