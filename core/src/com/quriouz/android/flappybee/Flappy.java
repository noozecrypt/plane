package com.quriouz.android.flappybee;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;

/**
 * Created by good on 09-06-2017.
 */
public class Flappy {

    private static final float COLLISION_RADIUS = 70;
    private final Circle collisionCircle;

    private float x = 0;
    private float y = 0;

    private static final float DIVE_ACCEL = 0.85F;
    private float ySpeed = 0;

    private static final float FLY_ACCEL = 18F;

    private Texture flappyTexture;




    public Flappy(Texture flappyTexture){

        this.flappyTexture = flappyTexture;
        collisionCircle = new Circle(x,y,COLLISION_RADIUS);
    }

    public void setPosition(float x, float y){
        this.x = x;
        this.y = y;
        updateCollisionCircle();

    }

    private void updateCollisionCircle(){
        collisionCircle.setX(x);
        collisionCircle.setY(y);
    }

    public void update(){
        ySpeed -= DIVE_ACCEL;
        setPosition(x,y+ySpeed);
    }

    public void flyUp(){
        ySpeed = FLY_ACCEL;
        setPosition(x,y+ySpeed);
    }

    public void drawDebug(ShapeRenderer shapeRenderer){
        shapeRenderer.circle(collisionCircle.x,collisionCircle.y,collisionCircle.radius);
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void draw(SpriteBatch batch) {

       float textureX = collisionCircle.x -
                flappyTexture.getWidth() / 2;
        float textureY = collisionCircle.y -
                flappyTexture.getHeight() / 2;


        batch.draw(flappyTexture, textureX,textureY);
    }

    public Circle getCollisionCircle() {
        return collisionCircle;
    }

    public void setflappyTexture(Texture texture) {
        flappyTexture = texture;
    }
}
