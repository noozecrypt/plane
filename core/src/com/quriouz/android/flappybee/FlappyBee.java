package com.quriouz.android.flappybee;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;


public class FlappyBee extends Game {

	private AssetManager assetManager = new AssetManager();

	@Override
	public void create() {
		setScreen(new StartScreen(this));
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}
}
