package com.quriouz.android.flappybee;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.*;
/**
 * Created by good on 09-06-2017.
 */
public class Flower {

    private static final float COLLISION_RECTANGLE_WIDTH = 60f;
    private static final float COLLISION_RECTANGLE_HEIGHT = 1341f;
    private static final float COLLISION_CIRCLE_RADIUS = 100;
    public static final float WIDTH = COLLISION_CIRCLE_RADIUS*2 ;


    private final Circle floorCollisionCircle;
    private final Rectangle floorCollisionRectangle;
    private final Circle ceilingCollisionCircle;
    private final Rectangle ceilingCollisionRectangle;

    private final Texture floorTexture;
    private final Texture ceilingTexture;

    private static final float MAX_SPEED_PER_SECOND = 350f;

    private static final float HEIGHT_OFFSET = -1200f;

    private static final float DISTANCE_BETWEEN_FLOOR_AND_CEILING = 585f;

    private boolean pointClaimed = false;

    public boolean isPointClaimed(){
        return pointClaimed;
    }

    public void markPointClaimed(){
        pointClaimed = true;
    }

    private float x =0;
    private float y= 0;


    public Flower(Texture floorTexture, Texture ceilingTexture) {
        this.floorTexture = floorTexture;
        this.ceilingTexture = ceilingTexture;
        this.y = MathUtils.random(HEIGHT_OFFSET);
        this.floorCollisionRectangle = new Rectangle(x, y,
                COLLISION_RECTANGLE_WIDTH, COLLISION_RECTANGLE_HEIGHT);
        this.floorCollisionCircle = new Circle(x +
                floorCollisionRectangle.width / 2, y +
                floorCollisionRectangle.height, COLLISION_CIRCLE_RADIUS);
        this.ceilingCollisionRectangle = new Rectangle(x,
                floorCollisionCircle.y + DISTANCE_BETWEEN_FLOOR_AND_CEILING,
                COLLISION_RECTANGLE_WIDTH, COLLISION_RECTANGLE_HEIGHT);
        this.ceilingCollisionCircle = new Circle(x +
                ceilingCollisionRectangle.width / 2,
                ceilingCollisionRectangle.y, COLLISION_CIRCLE_RADIUS);
    }

    public void update(float delta){
        setPosition(x-(MAX_SPEED_PER_SECOND*delta));
    }

    public void setPosition(float x){
        this.x = x;
        updateCollisionCircle();
        updateCollisionRectangle();

    }

    private void updateCollisionRectangle() {
        floorCollisionRectangle.setX(x);
        ceilingCollisionRectangle.setX(x);
    }

    private void updateCollisionCircle() {
        floorCollisionCircle.setX(x+ floorCollisionRectangle.width/2);
        ceilingCollisionCircle.setX(x + ceilingCollisionRectangle.width /2);
    }

    public void draw(SpriteBatch batch) {
        drawFloorFlower(batch);
        drawCeilingFlower(batch);
    }

    private void drawFloorFlower(SpriteBatch batch) {
        float textureX = floorCollisionCircle.x -
                floorTexture.getWidth() / 2;
        float textureY = floorCollisionRectangle.getY();
        batch.draw(floorTexture, textureX, textureY);
    }

    private void drawCeilingFlower(SpriteBatch batch) {
        float textureX = ceilingCollisionCircle.x -
                ceilingTexture.getWidth() / 2;
        float textureY = ceilingCollisionRectangle.getY()-COLLISION_CIRCLE_RADIUS;
        batch.draw(ceilingTexture, textureX, textureY);
    }

    public void drawDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.circle(floorCollisionCircle.x,
                floorCollisionCircle.y, floorCollisionCircle.radius);
        shapeRenderer.rect(floorCollisionRectangle.x,
                floorCollisionRectangle.y, floorCollisionRectangle.width,
                floorCollisionRectangle.height);
        shapeRenderer.circle(ceilingCollisionCircle.x,
                ceilingCollisionCircle.y, ceilingCollisionCircle.radius);
        shapeRenderer.rect(ceilingCollisionRectangle.x,
                ceilingCollisionRectangle.y, ceilingCollisionRectangle.width,
                ceilingCollisionRectangle.height);
    }

    public boolean isFlappyColliding(Flappy flappy){
        Circle flappyCollisionCircle = flappy.getCollisionCircle();
        return
                Intersector.overlaps(flappyCollisionCircle,ceilingCollisionCircle)
                        || Intersector.overlaps(flappyCollisionCircle, floorCollisionCircle)
                        || Intersector.overlaps(flappyCollisionCircle, ceilingCollisionRectangle)
                        || Intersector.overlaps(flappyCollisionCircle, floorCollisionRectangle);


    }

    public float getX() {
        return x;
    }


}
