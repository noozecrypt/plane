package com.quriouz.android.flappybee;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;



/**
 * Created by good on 09-06-2017.
 */
public class GameScreen extends ScreenAdapter {

    private Texture background;

    private static final float WORLD_WIDTH = 1080;
    private static final float WORLD_HEIGHT = 1920;
    private static final float GAP_BETWEEN_FLOWERS = 500F ;

    private int score;

    public static final String PREF_NAME_SCORE = "prefScore";

    public static final String PREF_HIGH_SCORE = "highScore";

    private ShapeRenderer shapeRenderer;
    private Viewport viewport;
    private Camera camera;
    private SpriteBatch batch;

    private Flappy flappy;
    private FlappyBee flappyBee;
    private Array<Flower> flowers = new Array<Flower>();

    private GlyphLayout glyphLayout;

    private Preferences prefs;

    private Texture[] flagsTop;
    private Texture[] flagsBottom;
    private Texture flappyTexture;

    private int flagCount;

    private BitmapFont bitmapFont;

    private boolean collisionFlag;

    private boolean startFly;

    private float TIMER = 3f;

    private Sound tapSound;

    private Sound gateSound;

    private Sound crashSound;

    Music flySound;

    Music bgMusic;

    public GameScreen(FlappyBee flappyBee,Music bgMusic) {
        this.flappyBee = flappyBee;
        prefs= Gdx.app.getPreferences(PREF_NAME_SCORE);
        tapSound = flappyBee.getAssetManager().get("sounds/tap.mp3", Sound.class);
        crashSound = flappyBee.getAssetManager().get("sounds/crash.mp3",Sound.class);
        flySound = flappyBee.getAssetManager().get("sounds/fly.mp3");
        gateSound = flappyBee.getAssetManager().get("sounds/gate.mp3",Sound.class);
        flySound.play();
        flySound.setLooping(true);
        this.bgMusic = bgMusic;
        flagCount = 0;
        flagsBottom =new Texture[12];
        flagsTop = new Texture[12];
        collisionFlag = false;
        startFly = false;
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }
    @Override
    public void show() {

        bitmapFont = new BitmapFont(Gdx.files.internal("score.fnt"));

        background = flappyBee.getAssetManager().get("bg.png");

        for(int i=0;i<=11;i++){
            flagsBottom[i] =
                    flappyBee.getAssetManager().get("flags/flagBottom_"  +  i +".png");
            flagsTop[i] =
                    flappyBee.getAssetManager().get("flags/flagTop_" + i +".png");
        }

        flappyTexture =
                flappyBee.getAssetManager().get("bee.png");

        glyphLayout = new GlyphLayout();
        camera = new OrthographicCamera();
        camera.position.set(WORLD_WIDTH / 2, WORLD_HEIGHT / 2, 0);
        camera.update();
        viewport = new FitViewport(WORLD_WIDTH, WORLD_HEIGHT, camera);
        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();
        flappy = new Flappy(flappyTexture);
        flappy.setPosition(WORLD_WIDTH/4,WORLD_HEIGHT/2);

    }
    @Override
    public void render(float delta) {
        clearScreen();
        draw();
        if(!startFly)
            if(Gdx.input.isKeyPressed(Input.Keys.SPACE)|| Gdx.input.justTouched()){
                startFly = true;
            }
        shapeRenderer.setProjectionMatrix(camera.projection);
        shapeRenderer.setTransformMatrix(camera.view);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        //drawDebug();
        shapeRenderer.end();
        update(delta);
    }

    public void draw(){
        batch.setProjectionMatrix(camera.projection);
        batch.setTransformMatrix(camera.view);
        batch.begin();
        batch.draw(background,0,0);
        drawFlowers();
        flappy.draw(batch);
        drawScore();
        batch.end();
    }

    private void update(float delta) {
        if(collisionFlag){
            afterCollision(delta);
            return;
        }

        if (startFly)updateFlappy(delta);
        updateFlowers(delta);
        updateScore();
        if(checkForCollision()){
            collisionFlag = true;
            Texture texture = flappyBee.getAssetManager().get("bee90.png");
            flappy.setflappyTexture(texture);
            flySound.pause();
            crashSound.play(1.0f);
        }

    }

    private void updateFlappy(float delta) {
        flappy.update();
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)|| Gdx.input.justTouched()){
            tapSound.play(0.65f);
            flappy.flyUp();
        }
        blockFlappyLeavingTheWorld();
    }

    private void afterCollision(float delta){
        TIMER-=delta;
        flappy.update();
        if(TIMER<=0){
            collisionFlag = false;
            bgMusic.pause();
            if(score>prefs.getInteger(PREF_HIGH_SCORE,0)){
                prefs.putInteger(PREF_HIGH_SCORE,score);
                prefs.flush();
            }
            flappyBee.setScreen(new EndScreen(flappyBee,score,prefs.getInteger(PREF_HIGH_SCORE,0),bgMusic));
        }
    }


    private void updateFlowers(float delta) {
        for(Flower flower:flowers){
            flower.update(delta);
        }
        checkIfNewFlowerIsNeeded();
        removeFlowersIfPassed();
    }

    private void blockFlappyLeavingTheWorld(){
        flappy.setPosition(flappy.getX(), MathUtils.clamp(flappy.getY(),0,WORLD_HEIGHT));
    }

    private void createNewFlower(){
        if(flagCount==12){
            flagCount=0;
        }
        Flower newFlower = new Flower(flagsBottom[flagCount],flagsTop[flagCount]);
        flagCount++;
        newFlower.setPosition(WORLD_WIDTH + Flower.WIDTH);
        flowers.add(newFlower);
    }

    private void checkIfNewFlowerIsNeeded(){
        if(flowers.size==0){
            createNewFlower();
        }else{
            Flower flower = flowers.peek();
            if(flower.getX()<WORLD_WIDTH-GAP_BETWEEN_FLOWERS){
                createNewFlower();
            }
        }
    }

    private void removeFlowersIfPassed(){
        if(flowers.size>0){
            Flower firstFlower = flowers.first();
            if (firstFlower.getX()<-Flower.WIDTH){
                flowers.removeValue(firstFlower,true);
            }
        }
    }

    private boolean checkForCollision(){
        for(Flower flower : flowers){
            if (flower.isFlappyColliding(flappy)){
                return true;
            }
        }
        return false;
    }

    private void updateScore(){
        Flower flower = flowers.first();
        if(flower.getX()<flappy.getX() && !flower.isPointClaimed()){

            flower.markPointClaimed();
            score++;
            gateSound.play(1.0f);
        }
    }

    private void drawFlowers(){
        for (Flower flower : flowers) {
            flower.draw(batch);
        }
    }

    private void drawScore() {
        String scoreAsString = Integer.toString(score);
        glyphLayout.setText(bitmapFont, scoreAsString);
        bitmapFont.draw(batch, scoreAsString,(viewport.getWorldWidth()
                - glyphLayout.width) / 2, (4 * viewport.getWorldHeight() / 5) -
                glyphLayout.height / 2);
    }

    private void drawDebug(){
        flappy.drawDebug(shapeRenderer);
        for(Flower flower: flowers){
            flower.drawDebug(shapeRenderer);
        }
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g,
                Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }
}
