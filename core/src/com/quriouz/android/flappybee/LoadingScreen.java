package com.quriouz.android.flappybee;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by good on 10-06-2017.
 */
public class LoadingScreen extends ScreenAdapter{

    private static final float WORLD_WIDTH = 1080;
    private static final float  WORLD_HEIGHT = 1920;

    private static final float PROGRESS_BAR_WIDTH=225;
    private static final float PROGRESS_BAR_HEIGHT =75;

    private ShapeRenderer shapeRenderer;
    private Viewport viewport;
    private Camera camera;
    private float progress=0;
    private final FlappyBee flappyBee;

    private Sound hornSound;

    private Sound loadSound;

    Music music;




    public LoadingScreen(FlappyBee flappyBee, Sound loadSound, Music music) {
        this.flappyBee = flappyBee;
        this.loadSound = loadSound;
        this.music = music;

    }

    public void resize(int width,int height){
        viewport.update(width,height);
    }

    public void show(){

        camera = new OrthographicCamera();
        camera.position.set(WORLD_WIDTH / 2, WORLD_HEIGHT / 2, 0);
        camera.update();
        viewport = new FitViewport(WORLD_WIDTH,WORLD_HEIGHT,camera);
        shapeRenderer = new ShapeRenderer();
        flappyBee.getAssetManager().load("bg.png",
                Texture.class);
        flappyBee.getAssetManager().load("bee.png",
                Texture.class);
        flappyBee.getAssetManager().load("bee90.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagTop_0.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_0.png",Texture.class);

        flappyBee.getAssetManager().load("flags/flagTop_1.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_1.png",Texture.class);

        flappyBee.getAssetManager().load("flags/flagTop_2.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_2.png",Texture.class);

        flappyBee.getAssetManager().load("flags/flagTop_3.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_3.png",Texture.class);

        flappyBee.getAssetManager().load("flags/flagTop_4.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_4.png",Texture.class);

        flappyBee.getAssetManager().load("flags/flagTop_5.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_5.png",Texture.class);

        flappyBee.getAssetManager().load("flags/flagTop_6.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_6.png",Texture.class);

        flappyBee.getAssetManager().load("flags/flagTop_7.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_7.png",Texture.class);

        flappyBee.getAssetManager().load("flags/flagTop_8.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_8.png",Texture.class);

        flappyBee.getAssetManager().load("flags/flagTop_9.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_9.png",Texture.class);

        flappyBee.getAssetManager().load("flags/flagTop_10.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_10.png",Texture.class);

        flappyBee.getAssetManager().load("flags/flagTop_11.png",Texture.class);
        flappyBee.getAssetManager().load("flags/flagBottom_11.png",Texture.class);

        flappyBee.getAssetManager().load("sounds/tap.mp3",Sound.class);
        flappyBee.getAssetManager().load("sounds/crash.mp3",Sound.class);
        flappyBee.getAssetManager().load("sounds/fly.mp3",Music.class);

        flappyBee.getAssetManager().load("gameOver.png",Texture.class);
        flappyBee.getAssetManager().load("highScore.png",Texture.class);
        flappyBee.getAssetManager().load("yourScore.png",Texture.class);

        flappyBee.getAssetManager().load("sounds/horn.mp3",Sound.class);
        flappyBee.getAssetManager().load("sounds/sad.mp3",Music.class);

        flappyBee.getAssetManager().load("sounds/gate.mp3",Sound.class);

        flappyBee.getAssetManager().load("restart.png",Texture.class);
        flappyBee.getAssetManager().load("restartPress.png",Texture.class);

        flappyBee.getAssetManager().load("home.png",Texture.class);
        flappyBee.getAssetManager().load("homePress.png",Texture.class);

        flappyBee.getAssetManager().load("board.png",Texture.class);

    }

    public void render(float delta){
        update();
        clearScreen();
        draw();
    }

    private void draw() {
        shapeRenderer.setProjectionMatrix(camera.projection);
        shapeRenderer.setTransformMatrix(camera.view);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.rect(
                (WORLD_WIDTH - PROGRESS_BAR_WIDTH) / 2, (WORLD_HEIGHT -
                        PROGRESS_BAR_HEIGHT) / 2,
                progress * PROGRESS_BAR_WIDTH, PROGRESS_BAR_HEIGHT);
        shapeRenderer.end();
    }

    private void clearScreen() {
        Gdx.gl.glClearColor(Color.BLACK.r, Color.BLACK.g,
                Color.BLACK.b, Color.BLACK.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    private void update() {
        if(flappyBee.getAssetManager().update()){
            flappyBee.setScreen(new GameScreen(flappyBee,music));
            loadSound.stop();
            music.play();
            hornSound = flappyBee.getAssetManager().get("sounds/horn.mp3",Sound.class);
            hornSound.play(1.0f);
        }else{
            progress = flappyBee.getAssetManager().getProgress();
        }

    }

    public void dispose(){
        shapeRenderer.dispose();
    }


}
