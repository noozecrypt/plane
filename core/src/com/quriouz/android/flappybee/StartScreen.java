package com.quriouz.android.flappybee;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.util.Random;

/**
 * Created by good on 10-06-2017.
 */
public class StartScreen extends ScreenAdapter{

    private static final float WORLD_WIDTH = 1080;
    private static final float WORLD_HEIGHT = 1920;

    private Stage stage;

    private Texture backgroundTexture;

    private Texture playTexture;
    private  Texture playPressTexture;


    private final FlappyBee game;


    private Sound loadSound = Gdx.audio.newSound(Gdx.files.internal("sounds/load.mp3"));

    Music back_0 = Gdx.audio.newMusic(Gdx.files.internal
            ("sounds/back_0.mp3"));

    Music back_1 = Gdx.audio.newMusic(Gdx.files.internal
            ("sounds/back_1.mp3"));

    Music current;

    Random rand = new Random();


    public StartScreen(FlappyBee game) {
        this.game = game;
        if(rand.nextInt(2)==0){
            back_0.play();
            back_0.setLooping(true);
            current = back_0;
        }else {
            back_1.play();
            back_1.setLooping(true);
            current = back_1;
        }

    }

    @Override
    public void show() {
        stage = new Stage(new FitViewport(WORLD_WIDTH, WORLD_HEIGHT));
        Gdx.input.setInputProcessor(stage);

        backgroundTexture = new Texture(Gdx.files.internal("bg_title.png"));
        playTexture = new Texture(Gdx.files.internal("play.png"));
        playPressTexture = new Texture(Gdx.files.internal("playPress.png"));

        Image background = new Image(backgroundTexture);
        background.setPosition(0,0);
        stage.addActor(background);



        ImageButton play = new ImageButton(new TextureRegionDrawable(new
                TextureRegion(playTexture)), new TextureRegionDrawable(new
                TextureRegion(playPressTexture)));
        play.setPosition(WORLD_WIDTH / 2, WORLD_HEIGHT / 2, Align.center);
        play.addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                super.tap(event, x, y, count, button);
                current.pause();
                loadSound.play(1.0f);
                game.setScreen(new LoadingScreen(game, loadSound, current));
                dispose();
            }
        });
        stage.addActor(play);

    }

    public void resize(int width, int height) {
        stage.getViewport().update(width,height,true);
    }

    public void render(float delta){
        stage.act(delta);
        stage.draw();
    }


    @Override
    public void dispose() {
        stage.dispose();
        backgroundTexture.dispose();
        playPressTexture.dispose();
        loadSound.dispose();
    }

}
